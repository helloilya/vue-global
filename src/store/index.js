import setting from './modules/setting';
import weather from './modules/weather';
import { createStore } from 'vuex';

// @todo: how to migrate on TS
// link: medium.com/better-programming/the-state-of-typed-vuex-the-cleanest-approach-2358ee05d230

// @todo: how to connect with Firebase
// link: antonioufano.com/articles/use-vuexfire-to-simplify-your-application-state-management

const store = createStore({
	modules: {
		setting,
		weather,
	},
	strict: process.env.NODE_ENV !== 'production',
});

export default store;