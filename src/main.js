import App from '@/App.vue';
import router from '@/router';
import store from '@/store';
import { createApp } from 'vue';

const app = createApp(App);

app.config.performance = process.env.NODE_ENV !== 'production';

// @todo: how to migrate on Vue3 and support TS
// link: johnpapa.net/vue2-to-vue3 and blog.logrocket.com/vue-typescript-tutorial-examples

app.use(router);
app.use(store);
app.mount('#app');