/**
 * The root path for api call.
 * @const {string}
 */
const GEO_API_PATH = '//geodb-free-service.wirefreethought.com/v1/geo/';

/**
 * The openWeatherMap API key.
 * @const {string}
 */
const WEATHER_API_KEY = '1be27354825d85769dfe157bafd59a92';

/**
 * The root path for api call.
 * @const {string}
 */
const WEATHER_API_PATH = '//api.openweathermap.org/data/2.5/';

export {
	GEO_API_PATH,
	WEATHER_API_KEY,
	WEATHER_API_PATH,
};