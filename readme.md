# Vue Global

Please use the following [cli](https://cli.vuejs.org/config/) commands:

```
npm run serve
npm run build
npm run lint
npm run test:unit
```

The data for weather based on [OpenWeather](https://openweathermap.org/api) and [GeoDB](http://geodb-cities-api.wirefreethought.com/). The key has been generated and ready for the production. Credential to log in is `nevedimkia@yandex.ru` with guest password.

### Forms

Validation based on [vee-validate](https://logaretm.github.io/vee-validate/).

### ESlint

Configuration options for eslint can be found [here](https://eslint.vuejs.org/rules/). The docs for list rules can be fount on [eslint](https://eslint.org/docs/rules/) or [github](https://github.com/benmosher/eslint-plugin-import/tree/master/docs/rules).

### Changelog

* 0.1.0 — Initial release.

### Author

Developed by [Ilya Fedotov](http://fedotov.me).